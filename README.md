# Запуск приложения
Для запуска приложения необходимо сделать публикацию приложения в **Visual Studio 2017** или более поздней версии, далее папку с опубликованным приложением необходимо разместить на сервере в папке ``` /home/ ``` <br/>
**Публикацию приложения необходимо делать в отдельную от проекта папку** <br/>
Исполняемый файл приложения ``` GlpiTicketModule.dll ```

**ВАЖНО** <br/>
Для работы необходимо иметь разверную в сети систему GLPI. <br/> 
О том, заставить работать нашу систему с GLPI необходимо ознакомиться с документацией

# Настройки nginx
Для того чтобы открыть файл конфигурации nginx необходимо прописать в терминале ``` nano /etc/nginx/nginx.conf ```
Далее удалить все содержимое файла и добавить следующее:
``` 
server {
    listen 80; # указываем порт, по которому nginx будет слушать запросы
    location / {
        proxy_pass http://localhost:5000; # указываем порт нашего приложения
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection keep-alive;
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
} 
``` 
# Создание сервиса для автостарта приложения
для создания сервиса необходимо создать файл, с расширением ``` .service ``` в директории ``` /etc/systemd/system/ ``` и прописать в нем следующее:
```
[Unit]
Description=Example .NET Web API Application running on Ubuntu

[Service]
WorkingDirectory=/home/GlpiTicketModule #менять только это (директория в которой находится приложение)
ExecStart=/usr/bin/dotnet /home/GlpiTicketModule/GlpiTicketModule.dll  #и это (исполняемый файл приложение)
Restart=always
RestartSec=10 
SyslogIdentifier=dotnet-example
User=root # пользователь от которого запускается приложения
Environment=ASPNETCORE_ENVIRONMENT=Production 

[Install]
WantedBy=multi-user.target
```
Подробнее о публикации приложения можно прочитать по ссылке: https://metanit.com/sharp/aspnet5/20.1.php или https://docs.microsoft.com/ru-ru/aspnet/core/host-and-deploy/?view=aspnetcore-2.2 <br/>
Подробнее о запуске приложения можно прочитать по ссылке: https://habr.com/ru/post/332920/

Для консультации и связи можно написать в телеграмм ```@romik174``` или же email <174.pra@gmail.com> 