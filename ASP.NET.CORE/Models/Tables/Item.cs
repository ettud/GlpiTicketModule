﻿namespace ASP.NET.CORE.Models.Tables
{
    public class Item
    {
        public string Type { get; set; }
        public string OtherSerial { get; set; }

        public string SQLQueryGetId(string type) => 
            $"SELECT Id " +
            $"FROM glpi_{type} " +
            $"WHERE otherserial='{OtherSerial}'";
    }
}