﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET.CORE.Models.Tables
{
    public static class CommonUsersQuery
    {
        public static string SQLQueryGetList => $"SELECT * FROM glpi_users";
    }
}
