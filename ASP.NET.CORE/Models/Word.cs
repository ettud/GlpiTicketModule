﻿using ASP.NET.CORE.Models.Tables;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET.CORE.Models
{
    public static class Word
    {
        private static readonly string[] tableHeaderCells = new string[] { "Наименование", "Инвентарный номер" };

        private static readonly string[] headerParagraphs = new string[] { "Главному бухгалтеру ГБУЗ «ЧОКБ»", "Карасевой М.В.",
            "от начальника ОМИТ", "Юдина И.В", "Контактный телефон – 749-39-04" };

        private static readonly string[] bodyParagraphs = new string[] { "Служебная записка",
            "В связи производственной необходимостью прошу оформить передачу следующей техники" };

        private static readonly string[] footerParagraphs = new string[] { "Из отделения", "В отделение ", "Из отделения (П/Л) ", "В отделение ",
            $"Число {DateTime.Now.Day}.{DateTime.Now.Month}.{DateTime.Now.Year}", "Подпись ____________ Юдин И.В." };




        public static void CreateFile(string path, Item item, string received)
        {
            using (WordprocessingDocument document =
            WordprocessingDocument.Create(path, WordprocessingDocumentType.Document))
            {
                MainDocumentPart mainPart = document.AddMainDocumentPart();

                mainPart.Document = new Document();
                Body body = mainPart.Document.AppendChild(new Body());

                createHeader(body);
                createBody(body);
                createTable(body, null, null);
            }
        }

        private static void createHeader(Body body)
        {
            foreach (string str in headerParagraphs)
            {
                Paragraph paragraph = body.AppendChild(new Paragraph());

                ParagraphProperties paragraphProperties = new ParagraphProperties(new Justification
                {
                    Val = JustificationValues.Right
                });

                paragraph.Append(paragraphProperties);
            
                Run run = paragraph.AppendChild(new Run(new Text(str)));
            }
        }

        private static void createBody(Body body)
        {
            Paragraph paragraph = body.AppendChild(new Paragraph());

            ParagraphProperties paragraphProperties = new ParagraphProperties(new Justification
            {
                Val = JustificationValues.Center
            });

            paragraph.Append(paragraphProperties);

            Run run = paragraph.AppendChild(new Run());
            run.AppendChild(new Text(bodyParagraphs.First()));


            paragraph = body.AppendChild(new Paragraph());

            paragraphProperties = new ParagraphProperties(new Justification
            {
                Val = JustificationValues.Left
            });

            paragraph.Append(paragraphProperties);

            run = paragraph.AppendChild(new Run(new Text(bodyParagraphs.Last())));
        }

        private static void createTable(Body body, Item item, string received)
        {
            TableCell headerCellTitle = new TableCell();

            headerCellTitle.Append(new Paragraph(
                new Run(
                    new Text(tableHeaderCells.First()))));

            TableCell headerCellSerial = new TableCell();

            headerCellTitle.Append(new Paragraph(
                new Run(
                    new Text(tableHeaderCells.First()))));

            TableRow row = new TableRow();
            row.Append(headerCellTitle);
            row.Append(headerCellSerial);

            Table table = new Table(row);

            TableProperties props = new TableProperties(
                new TableBorders(
                new TopBorder
                {
                    Val = new EnumValue<BorderValues>(BorderValues.Single),
                    Size = 12
                },
                new BottomBorder
                {
                    Val = new EnumValue<BorderValues>(BorderValues.Single),
                    Size = 12
                },
                new LeftBorder
                {
                    Val = new EnumValue<BorderValues>(BorderValues.Single),
                    Size = 12
                },
                new RightBorder
                {
                    Val = new EnumValue<BorderValues>(BorderValues.Single),
                    Size = 12
                },
                new InsideHorizontalBorder
                {
                    Val = new EnumValue<BorderValues>(BorderValues.Single),
                    Size = 12
                },
                new InsideVerticalBorder
                {
                    Val = new EnumValue<BorderValues>(BorderValues.Single),
                    Size = 12
                }));

            table.AppendChild<TableProperties>(props);

            body.Append(table);
        }
    }
}
