﻿import { print } from "util";

function parse_string_item_info() {
    var qrCodeValue = $('#qrCode').val();
    var fieldsItems = qrCodeValue.split('|');

    var Item = {
        otherserial: ParseFields(fieldsItems[1])
    };

    var user_sender = {
        username: 'Roman',
        name: 'Роман',
        surname: 'Прокопьев'
    }

    var user_recivier = {
        username: 'Roman',
        name: 'Роман',
        surname: 'Прокопьев'
    }

    var ticket = {
        name: "тестовая с клиента",
        content: "сделано на JS",
        date: "2018-05-18 15:00:00",
        priority: 1,
        status: 1,
        type: 1,
        userId: 0
    };

    var tuple = create_tuple(ticket, Item, user_sender, user_recivier);
    var data = JSON.stringify(tuple);
    post_request('ticket/add', data);
}

function parse_fields(field) {
    return field.split('=')[1];
}

function create_tuple(ticket, item, user_sender, user_recivier) {
    return {
        item1: ticket,
        item2: item,
        item3: user_sender,
        item4: user_recivier
    }
}

function post_request(action, data) {
    $.ajax({
        type: 'post',
        contentType: "application/json; charset=utf-8",
        url: action,
        data: data,
        success: function () {
            alert('Done');
        },
        error: function () {
            alert('Error')
        }
    });
}