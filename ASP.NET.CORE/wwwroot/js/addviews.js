﻿function get_view(url) {
    $.ajax({
        type: 'get',
        url: url,
        success: function (response) {
            var container = $('.container-ticket');
            container.html(response);
        },
        error: function () {
            alert('error')
        },
        contentType: "application/json; charset=utf-8"
    })
}