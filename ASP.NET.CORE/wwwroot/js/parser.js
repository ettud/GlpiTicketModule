﻿function parse_qrcode() {
    var qrCodeValue = $('#qrCode').val();
    var fields_items = qrCodeValue.split('|');

    $('.container-table').html(`
            <table class="table">
                Информация об устройстве
                <tr>
                    <th>Название</th>
                    <td>`+ parse_fields(fields_items[2]) +`</td>
                </tr>
                <tr>
                    <th>Инвентарный номер</th>
                    <td>`+ parse_fields(fields_items[1]) +`</td>
                </tr>
             </table>`);

    $('.container-dropdown').html(`
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Создание заявки <span class="caret"></span>
                </button>
            <ul class="dropdown-menu">
                <li><a href="#" onclick="get_view('createrepair');">на ремонт</a></li>
                <li><a href="#">на списание</a></li>
                <li><a href="#">на хранение</a></li>
            </ul>
        </div>`);

    //var device = {
    //    type:,
    //    name:,
    //    serial:parse_fields(fieldsItems[1])
    //};

    //var Item = {
    //    otherserial: parse_fields(fields_items[1])
    //};

    //var user_sender = {
    //    username: 'Roman',
    //    name: 'Роман',
    //    surname: 'Прокопьев'
    //}

    //var user_recivier = {
    //    username: 'Roman',
    //    name: 'Роман',
    //    surname: 'Прокопьев'
    //}

    //var ticket = {
    //    name: "тестовая с клиента",
    //    content: "сделано на JS",
    //    date: "2018-05-18 15:00:00",
    //    priority: 1,
    //    status: 1,
    //    type: 1,
    //    userId: 0
    //};

    //var tuple = create_tuple(ticket, Item, user_sender, user_recivier);
    //var data = JSON.stringify(tuple);
    //post_request('add', data);
}

function parse_fields(field) {
    return field.split('=')[1];
}

function create_tuple(ticket, item, user_sender, user_recivier) {
    return {
        item1: ticket,
        item2: item,
        item3: user_sender,
        item4:user_recivier
    }
}

function post_request(action, data) {
    $.ajax({
        type: 'post',
        contentType: "application/json; charset=utf-8",
        url: action,
        data: data,
        success: function () {
            alert('Done');
        },
        error: function () {
            alert('Error')
        }
    });
}