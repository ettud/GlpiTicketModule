﻿using System;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using ASP.NET.CORE.Models.Tables;

namespace ASP.NET.CORE.Controllers
{
    public class TicketController : MainController
    {     
        public IActionResult Add([FromBody] Tuple<Ticket, Item, User, User> container)
        {       
            int UserApplicantId;
            using (MySqlCommand GetUserId = new MySqlCommand(container.Item3.SQLQueryGetId, Connection))
            {
                MySqlDataReader reader = GetUserId.ExecuteReader();
                if (reader.Read())
                    UserApplicantId = (int)reader.GetValue(0);
                else return BadRequest("Invalid gettting applicant id");
            }

            int UserRecipientId;
            using (MySqlCommand GetUserId = new MySqlCommand(container.Item4.SQLQueryGetId, Connection))
            {
                MySqlDataReader reader = GetUserId.ExecuteReader();
                if (reader.Read())
                    UserRecipientId = (int)reader.GetValue(0);
                else return BadRequest("Invalid gettting applicant id");
            }

            using (MySqlCommand CreateTicket = new MySqlCommand(container.Item1.SQLQueryCreate, Connection))
            {
                if (CreateTicket.ExecuteNonQuery() == 0) return BadRequest("Error create ticket");
            }

            int TicketId;
            using (MySqlCommand GetIdTicket = new MySqlCommand(container.Item1.SQLQueryGetId, Connection))
            {
                MySqlDataReader reader = GetIdTicket.ExecuteReader();
                if (reader.Read())
                    TicketId = (int)reader.GetValue(0);
                else return BadRequest("Invalid getting ticket id");
            }

            int ItemId = FindIdItem(container.Item2);
            if (ItemId == 0) return BadRequest("Error get item id");

            using (MySqlCommand CreateTicketItems = new MySqlCommand(TicketItem.SQLQueryInsert(container.Item2.Type, ItemId, TicketId), Connection))
            {
                if (CreateTicketItems.ExecuteNonQuery() == 0) BadRequest("Error create note ticket-items");
            }     

            MySqlCommand CreateTicketUserApplicant = new MySqlCommand(UsersTickets.SQLQueryInsert(UserType.Applicant, TicketId, UserApplicantId), Connection);
            if (CreateTicketUserApplicant.ExecuteNonQuery() == 0) BadRequest("Error create note applicant-ticket");

            MySqlCommand CreateTicketUserRecipient = new MySqlCommand(UsersTickets.SQLQueryInsert(UserType.Recipient, TicketId, UserRecipientId), Connection);
            if (CreateTicketUserRecipient.ExecuteNonQuery() == 0) BadRequest("Error create note recipient-ticket");

            return Ok();
        }

        public IActionResult Index() => View("Views/Home/index.cshtml");

        public IActionResult CreateRepair() => View("Views/Tickets/RepairTicket.cshtml");

        private int FindIdItem(Item item)
        {
            MySqlCommand GetItemId = null;
            for (int i = 0; i < 3; i++)
            {
                switch (i)
                {
                    case 0:
                        GetItemId = new MySqlCommand(item.SQLQueryGetId("computers"), Connection);
                        item.Type = "Computer";
                        break;

                    case 1:
                        GetItemId = new MySqlCommand(item.SQLQueryGetId("printers"), Connection);
                        item.Type = "Printer";
                        break;

                    case 2:
                        GetItemId = new MySqlCommand(item.SQLQueryGetId("monitors"), Connection);
                        item.Type = "Monitor";
                        break;

                    default:
                        break;
                }

                MySqlDataReader reader = GetItemId.ExecuteReader();
                if (reader.Read())
                {
                    int id = (int)reader.GetValue(0);
                    reader.Close();
                    return id;
                }
                else
                {
                    reader.Close();
                    continue;
                }
            }

            return 0;
        }
    }
}
