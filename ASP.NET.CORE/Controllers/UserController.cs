﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP.NET.CORE.Models.Tables;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace ASP.NET.CORE.Controllers
{
    public class UserController : MainController
    {
        public UserController() : base() { }

        public IActionResult GetList()
        {
            MySqlCommand GetListCommand = new MySqlCommand(CommonUsersQuery.SQLQueryGetList, Connection);
            MySqlDataReader dataReader = GetListCommand.ExecuteReader();

            List<User> Users = new List<User>(); 
            while(dataReader.Read())
            {
                User temp = new User
                {
                    Username = dataReader.GetValue(1) as string,
                    Name     = dataReader.GetValue(6) as string,
                    Surname  = dataReader.GetValue(7) as string
                };

                Users.Add(temp);
            }

            return Ok(Users);
        }
    }
}